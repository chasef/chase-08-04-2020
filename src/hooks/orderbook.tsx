import { useContext } from 'react';
import { OrderbookContext } from '../providers/OrderbookProvider';
import { OrderbookContextType } from '../types/general';

export default function useOrderbook(): OrderbookContextType {
  const store = useContext(OrderbookContext);

  if (!store) {
    throw new Error('Cannot use useOrderbook outside of provider');
  }

  return store;
};