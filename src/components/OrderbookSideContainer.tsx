import { useMemo } from "react";
import OrderbookSideList from "./OrderbookSideList";
import useOrderbook from "../hooks/orderbook";
import { groupOrdersByTickSize } from "../helpers/orders";
import { isMobile } from "../helpers/responsiveness";
import OrderbookHeader from "./OrderbookHeader";

export default function OrderbookSideContainer() {
  const { orders, groupSelected, windowWidth } = useOrderbook();
  const groupedOrders = useMemo(() => groupOrdersByTickSize((orders), groupSelected), [orders, groupSelected]);
  const isMobileSize = isMobile(windowWidth);
  
  return (
    <div className="order-side-container">
      {!isMobileSize &&
        <>
          <OrderbookSideList orders={groupedOrders.bids} side={'bids'} depthBarFlipped={true} />
          <OrderbookSideList orders={groupedOrders.asks} side={'asks'} lowToHigh={true} />
        </>
      }
      {isMobileSize &&
        <>
          <OrderbookSideList orders={groupedOrders.asks} side={'asks'} lowToHigh={true} />
          <OrderbookHeader showSpreadOnly={true} />
          <OrderbookSideList orders={groupedOrders.bids} side={'bids'} />
        </>
      }
    </div>
  )
}