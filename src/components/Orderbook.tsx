import OrderbookHeader from "./OrderbookHeader";
import OrderbookSideContainer from "./OrderbookSideContainer";
import OrderbookFooter from "./OrderbookFooter";
import useOrderbook from "../hooks/orderbook";

export default function Orderbook() {
  const { windowWidth } = useOrderbook();

  return (
    <div className="orderbook">
      <OrderbookHeader />
      <OrderbookSideContainer />
      <OrderbookFooter />
    </div>
  )
}