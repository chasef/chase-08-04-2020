import React from 'react';
import { render } from '@testing-library/react';
import OrderbookSideListRow from './OrderbookSideListRow';
import { OrderWithTotal } from '../types/orders';

describe('component: OrderbookSideListRow', () => {
  test('renders order side list row component', () => {
    const order: OrderWithTotal = { price: 40000, size: 1000, total: 1000 };
    const { container } = render(<OrderbookSideListRow order={order} isBids={false} highestTotal={1000} depthBarFlipped={false} />);
    expect(container.firstChild).toMatchSnapshot();
  });
  
  test('renders order side list row component flipped', () => {
    const order: OrderWithTotal = { price: 40000, size: 1000, total: 1000 };
    const { container } = render(<OrderbookSideListRow order={order} isBids={true} highestTotal={1000} depthBarFlipped={true} />);
    expect(container.firstChild).toMatchSnapshot();
  });
});