import React from 'react';
import { render } from '@testing-library/react';
import OrderbookSideList from './OrderbookSideList';
import { OrderSideMap } from '../types/orders';

describe('component: OrderbookSideList', () => {
  test('renders orderbook side list bids component', () => {
    const orderMap: OrderSideMap = { '30000': 1000, '40000': 1000, '20000': 1000 };
    const { container } = render(
      <OrderbookSideList orders={orderMap} side={'bids'} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
  
  test('renders orderbook side list bids component flipped', () => {
    const orderMap: OrderSideMap = { '30000': 1000, '40000': 1000, '20000': 1000 };
    const { container } = render(
      <OrderbookSideList orders={orderMap} side={'bids'} depthBarFlipped={true} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
  
  test('renders orderbook side list asks component low to high', () => {
    const orderMap: OrderSideMap = { '30000': 1000, '40000': 1000, '20000': 1000 };
    const { container } = render(
      <OrderbookSideList orders={orderMap} side={'asks'} lowToHigh={true} />
    );
    expect(container.firstChild).toMatchSnapshot();
  });
});