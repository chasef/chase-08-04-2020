import React from 'react';
import { render } from '@testing-library/react';
import OrderbookFooter from './OrderbookFooter';
import userEvent from '@testing-library/user-event';
import OrderbookProvider from '../providers/OrderbookProvider';

describe('Orderbook Footer', () => {
  test('renders orderbook footer component', () => {
    const { container } = render(<OrderbookProvider><OrderbookFooter /></OrderbookProvider>);
    expect(container.firstChild).toMatchSnapshot();
  });

  test('toggles end and start feed button', () => {
    const { getByText } = render(<OrderbookProvider><OrderbookFooter /></OrderbookProvider>);
    const feedEnabledButton = getByText(/kill feed/i);
    expect(feedEnabledButton).not.toBeNull();
    userEvent.click(feedEnabledButton);
    expect(getByText(/start feed/i)).not.toBeNull();
  });
})