import { useMemo } from "react";
import { OrderSideMap } from "../types/orders";
import { covertMapToOrderArraySortedWithTotal, getHighestTotal } from "../helpers/orders";
import OrderbookSideListRow from "./OrderbookSideListRow";

interface Props {
  orders: OrderSideMap;
  side: 'bids' | 'asks';
  lowToHigh?: boolean;
  depthBarFlipped?: boolean;
}

export default function OrderbookSideList({ orders, side, lowToHigh = false, depthBarFlipped = false }: Props) {
  const columns = [{ name: 'total', key: 1 },{ name: 'size', key: 2 }, { name: 'price', key: 3 }];
  const isBids = side === 'bids';
  const sortedColumns = isBids ? columns : columns.reverse();
  const sortedOrdersWithTotal = useMemo(() => covertMapToOrderArraySortedWithTotal(orders, lowToHigh), [orders, lowToHigh]);
  const highestTotal = getHighestTotal(sortedOrdersWithTotal);

  return (
    <div className={`order-side-list ${side}`}>
      <div className="columns color-grey">
        {sortedColumns.map(column => <div className="columns-item" key={column.key}>{column.name.toUpperCase()}</div>)}
      </div>
      {orders && sortedOrdersWithTotal.map(order => {
        // Don't show orders with size zero
        if (order.size) {
          return (
            <OrderbookSideListRow 
              key={order.price} 
              order={order} 
              isBids={isBids} 
              highestTotal={highestTotal}
              depthBarFlipped={depthBarFlipped}
            />
          )
        }
      })}
    </div>
  )
}