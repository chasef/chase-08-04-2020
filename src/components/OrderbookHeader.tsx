import { useMemo } from "react";
import useOrderbook from "../hooks/orderbook";
import { TickGroup } from "../types/groups";
import { groupOrdersByTickSize, calculateSpread } from "../helpers/orders";

interface Props {
  showSpreadOnly?: boolean;
}

export default function OrderbookHeader({ showSpreadOnly = false }: Props) {
  const { orders, groupSelected, changeGroupSelected } = useOrderbook();
  const groupedOrders = useMemo(() => groupOrdersByTickSize((orders), groupSelected), [orders, groupSelected]);
  const spread = calculateSpread(groupedOrders);
  
  return (
    <div className="orderbook-header">
      {!showSpreadOnly && <div className="text-left">Order Book</div>}
      <div className="spread text-center color-grey">Spread { spread.toFixed(1) } {`(${groupSelected}%)`}</div>
      {!showSpreadOnly &&
        <div className="text-right">
          <select value={groupSelected} onChange={e => changeGroupSelected(parseFloat(e.target.value))}>
            <option value={TickGroup.Low}>{`Group ${TickGroup.Low}`}</option>
            <option value={TickGroup.Medium}>{`Group ${TickGroup.Medium}`}</option>
            <option value={TickGroup.High}>{`Group ${TickGroup.High}`}</option>
          </select>
        </div>
      }
    </div>
  )
}