import useOrderbook from "../hooks/orderbook";

export default function OrderbookFooter() {
  const { isFeedDisabled, toggleDisableFeed, toggleAssetFeed } = useOrderbook();

  return (
    <div className="orderbook-footer">
      <button className="purple" onClick={() => toggleAssetFeed()}>
        Toggle Feed
      </button>
      <button className="red" onClick={() => toggleDisableFeed()}>
        {isFeedDisabled ? "Start Feed" : "Kill Feed"}
      </button>
    </div>
  )
}