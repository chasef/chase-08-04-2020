import { formatPrice, numberWithCommas } from "../helpers/numbers";
import { OrderWithTotal } from "../types/orders";

interface Props {
  order: OrderWithTotal;
  isBids: boolean;
  highestTotal: number;
  depthBarFlipped: boolean;
}

const getColumnItems = (order: OrderWithTotal, isBids: boolean) => {
  let columnItems = [
    <div className="columns-item" key={1}>{numberWithCommas(order.total)}</div>,
    <div className="columns-item" key={2}>{numberWithCommas(order.size)}</div>,
    <div className={`columns-item ${isBids ? 'color-green' : 'color-red'}`} key={3}>{formatPrice(order.price)}</div>
  ];

  return isBids ? columnItems : columnItems.reverse();
};

export default function({ order, isBids, highestTotal, depthBarFlipped }: Props) {
  const percent = (order.total / highestTotal) * 100;

  return (
    <div className="order-side-list-row">
      <div className={`percentage-bg ${depthBarFlipped ? 'flipped' : ''}`} style={{width: `${percent}%`}} />
      <div className="columns">
        {getColumnItems(order, isBids).map(o => o)}
      </div>
    </div>
  )
}