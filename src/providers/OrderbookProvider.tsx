import { createContext, useEffect, useState } from 'react';
import { Asset, OrderbookContextType } from "../types/general";
import { TickGroup } from '../types/groups';
import { Order, FeedOrderList } from '../types/orders';
import { updateOrders, convertFeedOrderToFormattedOrder } from '../helpers/orders';

const initialState = {
  errorMessage: null,
  isFeedDisabled: false,
  toggleDisableFeed: () => {},
  toggleAssetFeed: () => {},
  groupSelected: TickGroup.Low,
  changeGroupSelected: () => {},
  orders: { asks: {}, bids: {} },
  windowWidth: 0
};

export const OrderbookContext = createContext<OrderbookContextType>(initialState);

type Props = {
  children: JSX.Element
};

export default function OrderbookProvider({ children }: Props) {
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const [isFeedDisabled, setIsFeedDisabled] = useState(false);
  const [disableFeedCount, setDisableFeedCount] = useState(0);
  const [ws, setWs] = useState<WebSocket | null>(null);
  const [assetSelected, setAssetSelected] = useState(Asset.Bitcoin);
  const [groupSelected, setGroupSelected] = useState(TickGroup.Low);
  const [orders, setOrders] = useState(initialState.orders);
  const [windowWidth, setWindowWidth] = useState<number>(window.innerWidth);

  useEffect(() => {
    const wsClient = new WebSocket('wss://www.cryptofacilities.com/ws/v1');
    wsClient.onopen = () => {
      setWs(wsClient);
    };
    wsClient.onclose = () => console.log('ws closed');
    wsClient.onerror = () => {
      setIsFeedDisabled(true);
      alert('Error occured');
    };

    return () => {
      wsClient.close();
    }
  }, []);

  useEffect(() => {
    if (!ws) return;

    if (assetSelected) {
      // Unsubscribe from current
      ws.send(JSON.stringify({
        "event":"unsubscribe",
        "feed":"book_ui_1",
        "product_ids":[assetSelected]
      }))

      // Subscribe to newly toggled
      ws.send(JSON.stringify({
        "event":"subscribe",
        "feed":"book_ui_1",
        "product_ids":[getOppositeAsset(assetSelected)]
      }))
    }

    ws.onmessage = e => {
      if (isFeedDisabled) return;
      const message = JSON.parse(e.data);
      // console.log('e', message);
      const { asks, bids } = message;
      addFeedDataToOrders(asks, bids);
    };
  }, [ws, isFeedDisabled, assetSelected]);

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange);
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange);
    }
  }, []);

  function addFeedDataToOrders(asks: FeedOrderList = [], bids: FeedOrderList = []) {
    const newAsksFormatted: Order[] = asks.map(ask => convertFeedOrderToFormattedOrder(ask));
    const newBidsFormatted: Order[] = bids.map(bid => convertFeedOrderToFormattedOrder(bid));

    setOrders(previousOrders => {
      return updateOrders(previousOrders, newAsksFormatted, newBidsFormatted);
    });
  }

  function toggleDisableFeed():void {
    setErrorMessage(null);

    if (isFeedDisabled) {
      setIsFeedDisabled(false)
    }

    if (!isFeedDisabled) {
      try {
        setDisableFeedCount(disableFeedCount + 1);
        setIsFeedDisabled(true);
        throw new Error('Feed disabled')
      } catch(error) {
        setErrorMessage(error.message);
      }
    }
  }

  const getOppositeAsset = (asset: Asset):Asset => asset === Asset.Bitcoin ? Asset.Ethereum : Asset.Bitcoin;

  const toggleAssetFeed = ():void => {
    setOrders(initialState.orders);
    setAssetSelected(getOppositeAsset(assetSelected));
  };

  const changeGroupSelected = (group: TickGroup):void => setGroupSelected(group);
  
  function handleWindowSizeChange():void {
    setWindowWidth(window.innerWidth);
  }

  return (
    <OrderbookContext.Provider value={{ 
      errorMessage, 
      isFeedDisabled, 
      toggleDisableFeed, 
      toggleAssetFeed,
      groupSelected,
      changeGroupSelected,
      orders,
      windowWidth
    }}>
      {children}
    </OrderbookContext.Provider>
  )
}