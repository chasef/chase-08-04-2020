import { roundTo, minMax, numberWithCommas, formatPrice } from "./numbers";

describe('helpers: numbers', () => {
  it('rounds numbers', () => {
    expect(roundTo(1.3, 0.5)).toEqual(1.5);
    expect(roundTo(1.3, 1)).toEqual(1);
    expect(roundTo(1.3, 2.5)).toEqual(2.5);
  });
  
  it('gets min and max', () => {
    const numbers = [1.6, 2.4, 1.3, 5];
    expect(minMax(numbers, 'min')).toEqual(1.3);
    expect(minMax(numbers, 'max')).toEqual(5);
  });
  
  it('adds commas to numbers', () => {
    expect(numberWithCommas(30100)).toEqual('30,100');
    expect(numberWithCommas('30100')).toEqual('30,100');
    expect(numberWithCommas('30100.06')).toEqual('30,100.06');
    expect(numberWithCommas(30100.06)).toEqual('30,100.06');
  });
  
  it('formats price', () => {
    expect(formatPrice(30100)).toEqual('30,100.00');
    expect(formatPrice(30100.06)).toEqual('30,100.06');
  });
});