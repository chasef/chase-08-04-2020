import { Order, FeedOrder, OrderSideMap, OrdersList, OrdersMap } from "../types/orders";
import { 
  convertFeedOrderToFormattedOrder, 
  covertMapToOrderArraySortedWithTotal, 
  updateOrders, 
  groupOrdersByTickSize,
  calculateSpread, 
  getHighestTotal 
} from "./orders";

describe('helpers: orders', () => {
  it('converts feed of orders to formatted order', () => {
    const feedOrder: FeedOrder = [30000, 1000];
    const expectedResponse: Order = { price: 30000, size: 1000 };
    expect(convertFeedOrderToFormattedOrder(feedOrder)).toEqual(expectedResponse);
  });
  
  it('converts order map to sorted array with total', () => {
    const orderMap: OrderSideMap = { '30000': 1000, '40000': 1000, '20000': 1000 };
    const highToLowExpectedResponse: OrdersList = [
      { price: 40000, size: 1000, total: 1000 },
      { price: 30000, size: 1000, total: 2000 },
      { price: 20000, size: 1000, total: 3000 }
    ];
    const lowToHighExpectedResponse: OrdersList = [
      { price: 20000, size: 1000, total: 1000 },
      { price: 30000, size: 1000, total: 2000 },
      { price: 40000, size: 1000, total: 3000 }
    ];
    expect(covertMapToOrderArraySortedWithTotal(orderMap, false)).toEqual(highToLowExpectedResponse);
    expect(covertMapToOrderArraySortedWithTotal(orderMap, true)).toEqual(lowToHighExpectedResponse);
  });
  
  it('update orders', () => {
    const existingOrdersMap: OrdersMap = {
      asks: { '10000': 1000, '20000': 1000, '30000': 1000 },
      bids: { '40000': 1000, '50000': 1000, '60000': 1000 }
    };
    const newAsks: Order[] = [{ price: 10000, size: 2000 }, { price: 30000, size: 2000 }];
    const newBids: Order[] = [{ price: 40000, size: 2000 }, { price: 50000, size: 2000 }];
    const expectedResponse: OrdersMap = {
      asks: { '10000': 2000, '20000': 1000, '30000': 2000 },
      bids: { '40000': 2000, '50000': 2000, '60000': 1000 }
    };
    expect(updateOrders(existingOrdersMap, newAsks, newBids)).toEqual(expectedResponse);
  });
  
  it('group orders by tick size', () => {
    const existingOrdersMap: OrdersMap = {
      asks: { '10000.06': 1000, '20000': 1000, '30000.2': 1000 },
      bids: { '40000.45': 1000, '50000.67': 1000, '60000': 1000 }
    };
    const expectedResponse: OrdersMap = {
      asks: { '10000': 1000, '20000': 1000, '30000': 1000 },
      bids: { '40000.5': 1000, '50000.5': 1000, '60000': 1000 }
    };
    expect(groupOrdersByTickSize(existingOrdersMap, 0.5)).toEqual(expectedResponse);
  });
  
  it('calculate spread', () => {
    const orders: OrdersMap = {
      asks: { '10000': 1000, '20000': 1000, '30000': 1000 },
      bids: { '40000.5': 1000, '50000.5': 1000, '60000': 1000 }
    };
    const expectedResponse = 50000;
    expect(calculateSpread(orders)).toEqual(expectedResponse);
  });
  
  it('get highest total from sorted orders', () => {
    const orders: OrdersList = [
      { price: 30000, size: 1000, total: 2000 },
      { price: 40000, size: 1000, total: 1000 },
      { price: 20000, size: 1000, total: 3000 }
    ];
    expect(getHighestTotal(orders)).toEqual(3000);
  });
});