import produce from 'immer'
import { TickGroup } from '../types/groups';
import { FeedOrder, Order, OrdersList, OrderSideMap, OrdersMap, Orders } from "../types/orders";
import { roundTo, minMax } from './numbers';

export function convertFeedOrderToFormattedOrder(feedOrder: FeedOrder): Order {
  const [price, size] = feedOrder;
  return { price, size };
}

export function covertMapToOrderArraySortedWithTotal(orderMap: OrderSideMap, lowToHigh: boolean): OrdersList {
  let runningTotal = 0;

  return Object.keys(orderMap)
    .sort((a, b) => { // sort first so we can add totals and not run a map twice doing it after
      if (lowToHigh) {
        return parseFloat(a) - parseFloat(b);
      } else {
        return parseFloat(b) - parseFloat(a);
      }
    })
    .map(orderKey => {
      const price = parseFloat(orderKey);
      const size = orderMap[orderKey];
      runningTotal = runningTotal + size;
      return { price, size, total: runningTotal };
    });
}

export function updateOrders(previousOrders: OrdersMap, newAsks: Order[], newBids: Order[]): OrdersMap { // return OrdersMap
  return produce(previousOrders, draftPreviousOrders => {
    for (const newAsk of newAsks) {
      const priceStr = newAsk.price.toString();
      draftPreviousOrders.asks[priceStr] = newAsk.size;
    }

    for (const newBid of newBids) {
      const priceStr = newBid.price.toString();
      draftPreviousOrders.bids[priceStr] = newBid.size;
    }
  });
}


export function groupOrdersByTickSize(orders: OrdersMap, groupSize: TickGroup): OrdersMap {
  const groupedOrdersMap: OrdersMap = { asks: {}, bids: {} };
  const { asks, bids } = orders;

  return produce(groupedOrdersMap, draftPreviousGroupedOrdersMap => {
    for (const askKey in asks) {
      const roundedNumber = roundTo(parseFloat(askKey), groupSize).toString();
      draftPreviousGroupedOrdersMap.asks[roundedNumber] = asks[askKey];
    }

    for (const bidKey in bids) {
      const roundedNumber = roundTo(parseFloat(bidKey), groupSize).toString();
      draftPreviousGroupedOrdersMap.bids[roundedNumber] = bids[bidKey];
    }
  })
}

export function calculateSpread(groupedOrders: OrdersMap): number {
  const { asks, bids } = groupedOrders;
  const asksAsFloats = Object.keys(asks).map(a => parseFloat(a)).filter(a => asks[a] !== 0);
  const bidsAsFloats = Object.keys(bids).map(b => parseFloat(b)).filter(b => bids[b] !== 0);
  const minAsk = minMax(asksAsFloats, 'min');
  const maxBid = minMax(bidsAsFloats, 'max');

  if (minAsk && maxBid) {
    return Math.abs(minAsk - maxBid);
  } else {
    return 0;
  }
}

export function getHighestTotal(sortedOrders: OrdersList = []):number {
  const totals = sortedOrders.map(order => order.total)
  const max = minMax(totals, 'max');
  return max || 0;
}