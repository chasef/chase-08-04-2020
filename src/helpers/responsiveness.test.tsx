import { isMobile } from "./responsiveness";

describe('helpers: responsiveness', () => {
  it('checks if width is mobile', () => {
    expect(isMobile(500)).toEqual(true);
    expect(isMobile(1000)).toEqual(false);
  });
});