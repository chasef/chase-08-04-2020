export function roundTo(num: number, toAmount: number) {
  if (!toAmount) return num;
  return (Math.round(num / toAmount) * toAmount);
}

// This exists because Math.min and Math.max don't work well with large arrays
export function minMax(numbers: number[], type: 'min' | 'max') {
  if (!numbers) return;

  const sortedArray = numbers.sort((a, b) => {
    if (type === 'min') {
      return a - b;
    } else {
      return b - a;
    }
  });

  return sortedArray[0];
}

export function numberWithCommas(x: number | string): string {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function formatPrice(x: number): string {
  const withDecimals = x.toFixed(2);
  return numberWithCommas(withDecimals);
}