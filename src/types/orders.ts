export interface Order {
  price: number;
  size: number;
}

export interface OrderWithTotal extends Order {
  total: number;
}

export type OrdersList = OrderWithTotal[]

export interface Orders {
  asks: Order[];
  bids: Order[];
}

export interface OrderSideMap {
  [key: string]: number
}

export interface OrdersMap {
  asks: OrderSideMap;
  bids: OrderSideMap;
}

export type FeedOrder = [number, number];
export type FeedOrderList = FeedOrder[];