export enum TickGroup {
  Low = 0.5,
  Medium = 1,
  High = 2.5
}