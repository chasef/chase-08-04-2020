import { TickGroup } from "./groups";
import { OrdersMap } from "./orders";

export enum Asset {
  Bitcoin = 'PI_XBTUSD',
  Ethereum = 'PI_ETHUSD'
}

export interface OrderbookContextType {
  errorMessage: string | null;
  isFeedDisabled: boolean;
  toggleDisableFeed: () => void;
  toggleAssetFeed: () => void;
  groupSelected: TickGroup;
  changeGroupSelected: (group: TickGroup) => void;
  orders: OrdersMap;
  windowWidth: number;
}