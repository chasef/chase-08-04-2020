import React, { useEffect, useState } from 'react';
import './App.css';
import OrderbookProvider from './providers/OrderbookProvider';
import Orderbook from './components/Orderbook';

function App() {
  return (
    <div className="App">
      <OrderbookProvider>
        <Orderbook />
      </OrderbookProvider>
    </div>
  );
}

export default App;
